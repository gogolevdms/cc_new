import enUS from './en-us';
import rus from './ru';

export default {
  'en-us': enUS,
  'ru-ru': rus,
};
