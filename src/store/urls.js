const server = 'http://193.176.78.247:13579';
const getUserUrl = `${server}/user`;
const loginUrl = `${server}/auth/authReq`;
const confirmLoginUrl = `${server}/auth/authConf`;

const getOrderUrl = `${server}/order`;
const getOrdersUrl = `${server}/order/getAllOrders`;
const addOrderUrl = `${server}/order/addOrder`;
const editOrderUrl = `${server}/order/editOrder`;

export {
  server,
  getUserUrl,
  loginUrl,
  confirmLoginUrl,
  getOrderUrl,
  getOrdersUrl,
  addOrderUrl,
  editOrderUrl,
};
