class User {
  constructor(phone, customerRatingValue = 3, workerRatingValue = 3) {
    this.phone = phone;
    this.customerRatingValue = customerRatingValue;
    this.workerRatingValue = workerRatingValue;
  }
}

class Order {
  constructor(id, name, customer, category, description, priceLow, priceHigh, deadline, responses, attachment, attachmentPhoto, attachmentFiles, safeDealValue, distanceValue = '4 km', customerRatingValue = 2) {
    this.id = id;
    this.name = name;
    this.customer = customer;
    this.category = category;
    this.description = description;
    this.priceLow = priceLow;
    this.priceHigh = priceHigh;
    this.deadline = deadline;
    this.responses = responses;
    this.attachment = attachment;
    this.attachmentPhoto = attachmentPhoto;
    this.attachmentFiles = attachmentFiles;
    this.safeDealValue = safeDealValue;
    this.distanceValue = distanceValue;
    this.customerRatingValue = customerRatingValue;
  }
}

class OrderShortcutted {
  constructor(id, name, customer, category, description, priceLow, priceHigh, deadline = '12ч', distanceValue = '4 km', customerRatingValue = 3) {
    this.id = id;
    this.name = name;
    this.customer = customer;
    this.category = category;
    this.description = description;
    this.priceLow = priceLow;
    this.priceHigh = priceHigh;
    this.deadline = deadline;
    this.distanceValue = distanceValue;
    this.customerRatingValue = customerRatingValue;
  }
}

export {
  User,
  Order,
  OrderShortcutted,
};
