
function setOrder(state, order) {
  state.order = order;
}

function setOrders(state, orders) {
  state.orders = orders;
}

function updateResponse(state, responses) {
  state.order.responses = responses;
}

function updateOrder(state, orderPart) {
  state.order.description = orderPart.description;
  state.order.priceLow = orderPart.priceLow;
  state.order.priceHigh = orderPart.priceHigh;
}

export {
  setOrder,
  setOrders,
  updateOrder,
  updateResponse,
};
