import axios from 'axios';
import {
  getOrderUrl,
  getOrdersUrl,
  editOrderUrl,
  addOrderUrl,
} from 'src/store/urls';
import { Order, OrderShortcutted } from 'src/store/models';

async function fetchOrder({ commit }, payload) {
  commit('clearError', null, { root: true });
  commit('setLoading', true, { root: true });

  try {
    const res = await axios.post(getOrderUrl, { id: payload });
    const result = res.data.data;
    const order = new Order(
      result.id,
      result.name,
      result.customer,
      result.order_category,
      result.description,
      result.price_low,
      result.price_high,
      `${result.start_date} - ${result.end_date}`,
      result.responses,
      result.attached,
      result.attached.photos,
      result.attached.files,
      result.safe_deal,
    );

    commit('setOrder', order);
    commit('setLoading', false, { root: true });
  } catch (error) {
    commit('setError', error.message, { root: true });
    commit('setLoading', false, { root: true });

    throw error;
  }
}

async function orderResponse({ commit, store }, payload) {
  commit('clearError', null, { root: true });
  commit('setLoading', true, { root: true });

  const id = payload;
  const newResponses = store.order.responses;

  if (newResponses.indexOf(id) === -1) {
    newResponses.push(id);

    try {
      await axios.post(editOrderUrl, {
        id: store.order.id,
        data: { responses: newResponses },
      });

      commit('updateResponse', newResponses);
      commit('setLoading', false, { root: true });
    } catch (error) {
      commit('setError', error.message, { root: true });
      commit('setLoading', false, { root: true });

      throw error;
    }
  }
}

async function updateOrder({ commit, store }, payload) {
  commit('clearError', null, { root: true });
  commit('setLoading', true, { root: true });

  try {
    await axios.post(editOrderUrl, {
      id: store.order.id,
      data: {
        description: payload.description,
        price_low: payload.priceLow,
        price_high: payload.priceHigh,
      },
    });

    commit('updateOrder', payload);
    commit('setLoading', false, { root: true });
  } catch (error) {
    commit('setError', error.message, { root: true });
    commit('setLoading', false, { root: true });

    throw error;
  }
}

async function fetchAllOrders({ commit }) {
  commit('clearError', null, { root: true });
  commit('setLoading', true, { root: true });

  try {
    const res = await axios.post(getOrdersUrl);
    const result = res.data.data.map(obj => new OrderShortcutted(
      obj.id,
      obj.name,
      obj.customer,
      obj.order_category,
      obj.description,
      obj.price_low,
      obj.price_high,
    ));

    commit('setOrders', result);
    commit('setLoading', false, { root: true });
  } catch (error) {
    commit('setError', error.message, { root: true });
    commit('setLoading', false, { root: true });

    throw error;
  }
}

async function addOrder({ commit }, order) {
  try {
    await axios.post(addOrderUrl, order);
  } catch (error) {
    commit('setError', error.message, { root: true });

    throw error;
  }
}

export {
  fetchOrder,
  fetchAllOrders,
  orderResponse,
  updateOrder,
  addOrder,
};
