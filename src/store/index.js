import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';

import { Cookies } from 'quasar';

import {
  getUserUrl,
  loginUrl,
  confirmLoginUrl,
} from 'src/store/urls';
import { User } from 'src/store/models';

import orders from 'src/store/orders';

Vue.use(Vuex);

export default function ({ ssrContext }) {
  const cookies = process.env.SERVER ? Cookies.parseSSR(ssrContext) : Cookies;

  const Store = new Vuex.Store({
    modules: {
      orders,
    },
    state: {
      user: {},
      token: cookies.get('access_token') || '',
      loading: false,
      error: null,
    },
    getters: {
      isAuthenticated: state => !!state.token,
    },
    mutations: {
      setUser(state, user) {
        state.user = user;
      },
      setLoading(state, isLoading) {
        state.loading = isLoading;
      },
      setError(state, error) {
        state.error = error;
      },
      clearError(state) {
        state.error = null;
      },
      setToken(state, token) {
        state.token = token;
      },
      resetToken(state) {
        state.token = '';
      },
    },
    actions: {
      async fetchUser({ commit }, id) {
        commit('clearError');
        commit('setLoading', true);

        try {
          const res = await axios.post(getUserUrl, { id });
          const userPart = new User(`+${res.data.data.phone_country_code}${res.data.data.phone_number}`);
          const user = {
            ...userPart,
            ...res.data.data,
          };

          commit('setUser', user);
          commit('setLoading', false);
        } catch (error) {
          commit('setError', error.message);
          commit('setLoading', false);

          throw error;
        }
      },
      loginUser(context, data) {
        return new Promise((resolve, reject) => {
          axios.post(loginUrl, JSON.parse(data))
            .then(res => resolve(res))
            .catch(err => reject(err));
        });
      },
      confirmLogin({ commit, dispatch }, payload) {
        return new Promise((resolve, reject) => {
          axios.post(confirmLoginUrl, JSON.parse(payload))
            .then((res) => {
              cookies.set('id', res.data.data.id);
              cookies.set('access_token', res.data.data.access_token);
              cookies.set('refresh_token', res.data.data.refresh_token);

              commit('setToken', res.data.data.access_token);

              const id = cookies.get('id');
              dispatch('fetchUser', id);

              resolve(res);
            })
            .catch((err) => {
              cookies.remove('access_token');

              reject(err);
            });
        });
      },
      logoutUser({ commit }) {
        return new Promise((resolve) => {
          commit('resetToken');

          cookies.remove('access_token');
          cookies.remove('refresh_token');
          cookies.remove('id');

          resolve();
        });
      },
    },
  });

  if (process.env.DEV && module.hot) {
    module.hot.accept(['./orders'], () => {
      // eslint-disable-next-line
      const newOrders = require('./orders').default;

      Store.hotUpdate({
        modules: {
          orders: newOrders,
        },
      });
    });
  }

  return Store;
}
