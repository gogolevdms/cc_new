const EmptyLayout = import('layouts/EmptyLayout.vue');

const routes = [
  {
    path: '/login',
    component: () => EmptyLayout,
    children: [
      {
        path: '',
        component: () => import('pages/LoginPage.vue'),
        name: 'login',
      },
    ],
    meta: {
      requiresVisitor: true,
    },
  },
  {
    path: '/confirm',
    component: () => EmptyLayout,
    children: [
      {
        path: '',
        component: () => import('pages/ConfirmPage.vue'),
        name: 'confirm',
        props: true,
      },
    ],
    meta: {
      requiresVisitor: true,
    },
  },
  {
    path: '',
    component: () => import('layouts/Main.vue'),
    redirect: { name: 'profile' },
    children: [
      {
        path: 'profile',
        component: () => import('pages/ProfilePage.vue'),
        name: 'profile',
        props: true,
      },
      {
        path: 'order/:id',
        component: () => import('pages/OrderPage.vue'),
        name: 'order',
        props: true,
      },
      {
        path: 'account',
        component: () => import('pages/AccountPage.vue'),
        name: 'account',
        props: true,
      },
      {
        path: 'new',
        component: () => import('pages/NewOrderPage.vue'),
        name: 'new',
        props: true,
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  // otherwise redirect to home
  { path: '*', redirect: '/login' },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
