# Quasar App

## Commands for build
### Developing
>$ quasar dev -m ssr

..or the longer form:
>$ quasar dev --mode ssr

with a specific Quasar theme:
>$ quasar dev -m ssr -t ios

###Building for Production
>$ quasar build -m ssr

..or the longer form:
>$ quasar build --mode ssr

with a specific Quasar theme:
>$ quasar build -m ssr -t ios
